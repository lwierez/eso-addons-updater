#!/bin/python3

import json
import os
import re
import requests
from zipfile import ZipFile


# Compiled regex
COMP_RE = {
    # Find the version of an addon in its manifest
    "version_in_addon_manifest": re.compile(
        r"(?<=## Version: ).+"
    ),
    # Find the version of an addon on its ESOUI page
    "version_in_addon_page": re.compile(
        r"(?<=<div id=\"version\">Version: ).+(?=</div>)"
    ),
    # Find the download link on an ESOUI addon page
    "esoui_download_link": re.compile(
        (r'(?<=<div id="downloadbutton">\n'
            r'<div id="icon">\n'
            r'<a href=").+(?=">)')
    ),
    # Find the manual download link on an ESOUI download page
    # This is required because ESOUI does not provide a direct download link on
    # the addon pages
    "esoui_manual_download_link": re.compile(
        (r'(?<=<div class="manuallink">\nProblems with the download\? '
            r'<a href=").+'
            r'(?=">)')
    )
}


# Functions
# Look for all the required information about an addon
def parse_addon(addon):
    print("Fetching data for {0}".format(addon['name']))

    # First, we get the data from the addon manifest and the addon web page
    try:
        with open("{0}/{0}.txt".format(addon['folder'])) as manifest_file:
            manifest = manifest_file.read()
    # If the encoding is incorrect, an error is raised
    except UnicodeDecodeError:
        print("ERROR: Couldn't decode addon manifest")
        manifest = ""
    # If the manifest is not found, we'll install the addon
    except FileNotFoundError:
        print(("Couldn't find addon manifest. "
               "This script will ask you if you want it to install it"))
        # Generating a false manifest to let the regex works
        manifest = "## Version: not installed\n"
    if manifest == "":
        return

    web_page = requests.get(addon['url']).text

    # Then we can proceed and compare versions
    local_version = COMP_RE["version_in_addon_manifest"].search(
        manifest).group(0)
    remote_version = COMP_RE["version_in_addon_page"].search(web_page).group(0)
    is_outdated = local_version != remote_version
    print("Local version is {0}, remote is {1}. {2}".format(
        local_version,
        remote_version,
        "Addon is outdated" if is_outdated else "No action required"
    ))
    if is_outdated:
        choice = input(("Would you like this script to update {0} "
                        "from version {1} to {2}? [yes/no]\n").format(
            addon["name"], local_version, remote_version
        ))
        if choice in ["yes", "y"]:
            return (addon['name'], web_page, local_version, remote_version)

    return None


def retreive_file(action):
    download_link = "https://esoui.com{0}".format(
        COMP_RE["esoui_download_link"].search(action[1]).group(0)
        )
    download_link = re.sub("&amp;", "&", download_link)
    download_page_content = requests.get(
        download_link, allow_redirects=True).text
    manual_download_link = COMP_RE["esoui_manual_download_link"].search(
        download_page_content).group(0)
    addon_file = requests.get(manual_download_link)
    open("{0}.zip".format(action[0]), 'wb').write(addon_file.content)


def install_addon(action):
    print("Downloading {0}".format(action[0]))
    retreive_file(action)
    print("Extracting {0}".format(action[0]))
    with ZipFile("{0}.zip".format(action[0]), 'r') as archive:
        archive.extractall()
    os.remove("{0}.zip".format(action[0]))


# Script behavior
actions = []
with open('addons.json') as addons_json:
    addons = json.load(addons_json)
    for addon in addons['mods']:
        addon_res = parse_addon(addon)
        if addon_res is not None:
            actions.append(addon_res)

if len(actions) > 0:
    print("\nThe following addons will be updated:")
    for action in actions:
        print(" - {0} (from {1} to {2})".format(action[0], action[2], action[3]))
    choice = input("\nValidate? [yes/no] ")
    apply_actions = choice in ["yes", "y"]

    if apply_actions:
        for action in actions:
            install_addon(action)
else:
    print("\nNo action required.")
