# ESO AddOns updater

## Features
This script is an updater for addons downloaded from [ESOUI](https://www.esoui.com). It can also install your addons for you.

## Install and configure
The code is written is Python 3 and rely on a JSON file representing the addons called addons.json. To use the script, you should place it into you addons folder, and create your addons.json config file in the same directory.  

The JSON follows this structure:
```json
{
    "mods": [
        {
            // Addon 1
        },
        {
            // Addon 2
        },
        {
            // Addon 3
        }
    ]
}
```

And each addon follows this structure:
```json
{
    "name": "Name of the addon", // Can be what you want
    "folder": "Folder where the addon is installed", // MUST BE THE DEFAULT ADDON FOLDER NAME
    "url": "Link to the ESOUI page of the mod"
}
```

Like in this exemple:
```json
{
    "mods": [
        {
            "name": "Action Duration Reminder",
            "folder": "ActionDurationReminder",
            "url": "https://www.esoui.com/downloads/info1536-ActionDurationReminder.html"
        },
        {
            "name": "Combat Metrics",
            "folder": "CombatMetrics",
            "url": "https://www.esoui.com/downloads/info1360-CombatMetrics.html"
        }
    ]
}
```

## Use
Then, just launch the script!  
Each addon from your JSON file will be analyzed, and if an addon is missing or outdated, the script will ask you if you want to update/install it.
